﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ListLibrary
{
    public class CustomList<T>: IList<T>
    {
        private Item<T> head;
        private Item<T> tail;
        /// <summary>
        /// The property return number of elements contained in the CustomList
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the IList is read-only.
        /// Make it always false
        /// </summary>
        public bool IsReadOnly => false;

        public CustomList(params T[] values)
        {
            foreach (var value in values)
            {
                this.Add(value);
            }
        }

        public CustomList(IEnumerable<T> values)
        {
            foreach (var value in values)
            {
                this.Add(value);
            }
        }

        /// <summary>
        /// Get or set data at the position.
        /// </summary>
        /// <param name="index">Position</param>
        /// <exception cref="IndexOutOfRangeException">Throw when index is less than 0 or greater than Count - 1</exception>
        public T this[int index]
        {
            get
            {
                if (index > Count - 1 || index < 0)
                    throw new IndexOutOfRangeException();

                var current = head;
                for (int i = 0; i < index; i++)
                {
                    current = current.Next;
                }

                return current.Data;
            }
            set
            {
                if (index > Count - 1 || index < 0)
                    throw new IndexOutOfRangeException();

                var current = head;
                for (int i = 0; i < index; i++)
                {
                    current = current.Next;
                }

                current.Data = value;
            }

        }

        /// <summary>
        ///  Adds an object to the end of the CustomList.
        /// </summary>
        /// <param name="data">Object that should be added in the CustomList</param>
        /// <exception cref="ArgumentNullException">Throws when you try to add null</exception>
        public void Add(T data)
        {
            if (data == null)
                throw new ArgumentNullException();

            Item<T> item = new Item<T>(data);

            if (head == null)
            {
                head = item;
                tail = item;
            }
            else
                tail.Next = item;

            tail = item;
            Count++;

        }

        /// <summary>
        /// Removes all elements from the CustomList
        /// </summary>
        public void Clear()
        {
            head = null;
            tail = null;
            Count = 0;
        }

        /// <summary>
        /// Determines whether an element is in the CustomList
        /// </summary>
        /// <param name="item">Object we check to see if it is on the CustomLIst</param>
        /// <returns>True if the element exists in the CustomList, else false</returns>
        public bool Contains(T item)
        {
            var current = head;

            while (current != null)
            {
                if (current.Data.Equals(item))
                    return true;

                current = current.Next;
            }

            return false;
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the CustomList.
        /// </summary>
        /// <param name="item"> The object to remove from the CustomList</param>
        /// <returns>True if item is successfully removed; otherwise, false. This method also returns
        ///     false if item was not found in the CustomList.</returns>
        /// <exception cref="ArgumentNullException">Throws when you try to remove null</exception>
        public bool Remove(T item)
        {
            if (item == null)
                throw new ArgumentNullException();

            Item<T> previous = null;
            var current = head;

            while (current != null)
            {
                if (current.Data.Equals(item))
                {
                    if (previous != null)
                    {
                        previous.Next = current.Next;

                        if (current.Next == null)
                            tail = previous;
                    }
                    else
                    {
                        head = head.Next;

                        if (head == null)
                            tail = null;
                    }

                    Count--;
                    return true;
                }

                previous = current;
                current = current.Next;
            }

            return false;

        }

        /// <summary>
        /// Searches for the specified object and returns the zero-based index of the first
        ///     occurrence within the CustomList.
        /// </summary>
        /// <param name="item">The object whose index we want to get </param>
        /// <returns>The zero-based index of the first occurrence of item within the entire CustomList,
        ///    if found; otherwise, -1.</returns>
        public int IndexOf(T item)
        {
            var current = head;
            int index = 0;

            while (current != null)
            {
                if (current.Data.Equals(item))
                {
                    return index;
                }

                current = current.Next;
                index++;
            }
            return -1;

        }

        /// <summary>
        /// Inserts an element into the CustomList at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which item should be inserted.</param>
        /// <param name="item">The object to insert.</param>
        /// <exception cref="ArgumentOutOfRangeException">Throw when index is less than 0 or greater than Count - 1</exception>
        public void Insert(int index, T item)
        {
            if (index < 0 || index > Count)
                throw new ArgumentOutOfRangeException();
            if (item == null)
                throw new ArgumentNullException();

            Item<T> newItem = new Item<T>(item);

            var current = head;
            Item<T> previous = null;

            for (int i = 0; i < index; i++)
            {
                previous = current;
                current = current.Next;
            }

            if (previous == null)
            {
                var oldItem = head;
                head = newItem;
                newItem.Next = oldItem;
            }
            else
            {
                newItem.Next = current;
                previous.Next = newItem;
            }

            Count++;

        }

        /// <summary>
        /// Removes the element at the specified index of the CustomList.
        /// </summary>
        /// <param name="index">The zero-based index of the element to remove.</param>
        /// <exception cref="ArgumentOutOfRangeException">Throw when index is less than 0 or greater than Count - 1</exception>
        public void RemoveAt(int index)
        {
            if (index < 0 || index > Count - 1)
                throw new ArgumentOutOfRangeException();

            var current = head;
            Item<T> previous = null;

            for (int i = 0; i < index; i++)
            {
                previous = current;
                current = current.Next;
            }

            if (current == head)
            {
                head = head.Next;
                if (head == null)
                    tail = null;
            }
            else
            {
                if (current == tail)
                    tail = previous;

                previous.Next = current.Next;
            }
            Count--;

        }

        /// <summary>
        /// Copies the entire CustomList to a compatible one-dimensional array, starting at the beginning of the target array.
        /// </summary>
        /// <param name="array">The one-dimensional System.Array that is the destination of the elements copied
        ///     from CustomList</param>
        /// <param name="arrayIndex">The zero-based index in the source System.Array at which
        ///   copying begins.</param>
        ///   <exception cref="ArgumentNullException">Array is null.</exception>
        ///   <exception cref="ArgumentException">The number of elements in the source CustomList is greater
        ///    than the number of elements that the destination array can contain</exception>
        public void CopyTo(T[] array, int arrayIndex)
        {
            if (array == null)
                throw new ArgumentNullException();

            if (array.Length - arrayIndex < this.Count)
                throw new ArgumentException();

            var current = head;

            while (current != null)
            {
                array[arrayIndex] = current.Data;
                arrayIndex++;
                current = current.Next;
            }

        }

        /// <summary>
        /// Returns an enumerator that iterates through the CustomList.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<T> GetEnumerator()
        {
            var current = head;

            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }

        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }
    }
}
